﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Actuizador_Precios_OC
{
    public partial class Form1 : Form
    {
        public static class Compania
        {
            public static string CodCompania;
        }
        public static class RowOC
        {
            public static string RowOCCod;
        }

        public static class connetionString
        {
            public static string connetionStringC;
        }
        public static class OC
        {
            public static string CodOc;
        }
        public Form1(string  Ccia, string row)
        {
            Compania.CodCompania = Ccia;
            RowOC.RowOCCod = row;
            InitializeComponent();
            connetionString.connetionStringC = "Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=sa;Password=jda";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            
            SqlConnection cnn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;

           
            cnn = new SqlConnection(connetionString.connetionStringC);
            sql = "SELECT ORDEN_COMPRA FROM "+ Compania.CodCompania.ToString() + ".ORDEN_COMPRA WHERE RowPointer='" + RowOC.RowOCCod.ToString() + "';";

            try
            {

                cnn.Open();
                adapter.InsertCommand = new SqlCommand(sql, cnn);
                OC.CodOc=adapter.InsertCommand.ExecuteScalar().ToString();
                LblCodOc.Text = "Esta seguro de actualizar los precios de la orden de compra: " + OC.CodOc.ToString() + "?";
                cnn.Close();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

           

        }

        private void BtnSi_Click(object sender, EventArgs e)
        {
            SqlConnection cnn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            decimal porcentaje;
            cnn = new SqlConnection(connetionString.connetionStringC);
            sql = "SELECT Porcentaje FROM dbo.Porcentajes_Act_Precios WHERE Compania='" + Compania.CodCompania.ToString() + "';";

            try
            {

                cnn.Open();
                adapter.InsertCommand = new SqlCommand(sql, cnn);

                porcentaje = decimal.Parse(adapter.InsertCommand.ExecuteScalar().ToString());
                string SQL2 = null;
                SQL2 = "EXECUTE dbo.Act_Precios_OC @pais='"+Compania.CodCompania.ToString()+"',@OrdenCompra='"+ OC.CodOc.ToString()+ "',@porcentaje='"+porcentaje+"'";
                adapter.UpdateCommand = new SqlCommand(SQL2, cnn);
                adapter.UpdateCommand.ExecuteNonQuery();

                cnn.Close();
                MessageBox.Show("Exito!!!!");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BtnNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
