﻿namespace Actuizador_Precios_OC
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblCodOc = new System.Windows.Forms.Label();
            this.BtnSi = new System.Windows.Forms.Button();
            this.BtnNo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LblCodOc
            // 
            this.LblCodOc.Location = new System.Drawing.Point(13, 9);
            this.LblCodOc.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblCodOc.Name = "LblCodOc";
            this.LblCodOc.Size = new System.Drawing.Size(311, 45);
            this.LblCodOc.TabIndex = 0;
            this.LblCodOc.Text = "label1";
            this.LblCodOc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BtnSi
            // 
            this.BtnSi.Location = new System.Drawing.Point(84, 79);
            this.BtnSi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnSi.Name = "BtnSi";
            this.BtnSi.Size = new System.Drawing.Size(92, 43);
            this.BtnSi.TabIndex = 1;
            this.BtnSi.Text = "Si";
            this.BtnSi.UseVisualStyleBackColor = true;
            this.BtnSi.Click += new System.EventHandler(this.BtnSi_Click);
            // 
            // BtnNo
            // 
            this.BtnNo.Location = new System.Drawing.Point(224, 79);
            this.BtnNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnNo.Name = "BtnNo";
            this.BtnNo.Size = new System.Drawing.Size(110, 43);
            this.BtnNo.TabIndex = 2;
            this.BtnNo.Text = "No";
            this.BtnNo.UseVisualStyleBackColor = true;
            this.BtnNo.Click += new System.EventHandler(this.BtnNo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 135);
            this.Controls.Add(this.BtnNo);
            this.Controls.Add(this.BtnSi);
            this.Controls.Add(this.LblCodOc);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Grupo Emasal - Actualizar Precio OC";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblCodOc;
        private System.Windows.Forms.Button BtnSi;
        private System.Windows.Forms.Button BtnNo;
    }
}

